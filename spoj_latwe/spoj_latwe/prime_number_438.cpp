#include "prime_number_438.h"

using namespace std;

bool isPrime(int n) {
	if (n == 1) {
		return false;
	}

	for (int i = 2; i < n; ++i) {
		if (n%i == 0) {
			return false;
		}
	}

	return true;
}

void prime_number_438(void) {

	int ntests;
	cin >> ntests;
	int *tab = new int[ntests];

	for (int i = 0; i < ntests; ++i) {
		cin >> tab[i];
	}

	for (int i = 0; i < ntests; ++i) {
		if (isPrime(tab[i])) {
			cout << "TAK" << endl;
			continue;
		}
		cout << "NIE" << endl;
	}

	delete[] tab;
}