#include <iostream>

using namespace std;

unsigned long long int *cache = NULL; // 0 = number , 1 value
unsigned int cache_cnt = 0;
unsigned int ntests = 0;
unsigned long long int fact(unsigned int n) {
	static unsigned int nt;
	if (n < 2) {
		return 1;
	}

	unsigned long long int a = 0;
	unsigned long long int r = 2;
	for (unsigned int i = 0; i < nt * 2; i+=2) {
		if (cache[i] <= n && cache[i] > r) {
			if (cache[i] == n) {
				return cache[i + 1];
			}
			// szukam najblizszej n
			r = cache[i+1];
			a = cache[i];
			cout << " cache i + 1 " << r << endl;
			cout << " cache i " << a << endl;
		}
	}

	for (unsigned long int i = a + 1; i <= n; ++i) {
		r *= i;
		//cout << r << endl;
	}
	cache_cnt += 2;
	if (cache_cnt < 2 * ntests) {
		cache[cache_cnt-1] = n;
		cache[cache_cnt + 1] = r;
		cout << " dodaje cache i + 1 " << r << endl;
		cout << " dodaje cache i " << n << endl;
		nt++;
	}

	return r;
}
void factorial_496() {
	cin >> ntests;
	unsigned long int *tab = new unsigned long int[ntests];
	cache = new unsigned long long int[2*ntests];
	memset(cache, 0, sizeof(unsigned long long int) * 2 * ntests);

	for (int i = 0; i < ntests; ++i) {
		cin >> tab[i];
		//scanf("%d", tab + i);
		//tab[i] = i;
	}

	unsigned long long r;
	for (int i = 0; i < ntests; ++i) {
		r = fact(tab[i]);
		cout << ((r / 10) % (unsigned long long int)10) << " " << (r % (unsigned long long int)10) << endl;
	}

	delete[] tab;
	delete[] cache;
}